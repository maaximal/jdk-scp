FROM gradle:jdk11
USER root
RUN curl -sL https://deb.nodesource.com/setup_6.x | bash -
RUN apt-get update && apt-get install -y --no-install-recommends \
		openssh-server \
		netcat-openbsd \
		git \
		nodejs \
	&& apt-get upgrade -y \
	&& rm -rf /var/lib/apt/lists/* /var/cache/apt
	
RUN ln -s "$(which nodejs)" /usr/local/bin/node